from mnist import load_mnist

(x_train, t_train), (x_test, t_test) = load_mnist(normalize=False, flatten=True, one_hot_label=True)
print(x_train.shape, t_train.shape, x_test.shape, t_test.shape)
print(t_train)

